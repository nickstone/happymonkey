import json
import requests
import csv
import subprocess
import sys

class PyHEC:

    def __init__(self, token, uri, port='8088'):
        if not 'http' in uri:
            raise("no http or https found in hostname")
        self.token = token
        self.uri = uri+":"+port+"/services/collector/event"
        self.port = port

    """
    event data is the actual event data
    metadata are sourcetype, index, etc
    """
    def send(self, event, metadata=None):
        headers = {'Authorization': 'Splunk '+self.token}

        payload = {"host": self.uri,
                   "event": event}

        if metadata:
            payload.update(metadata)

        r = requests.post(self.uri, data=json.dumps(payload), headers=headers, verify=False if 'https' in self.uri else False)

        return r.status_code, r.text,

def main():
	subprocess.call('clear')
	#check_os()

	if len(sys.argv) == 1:
		print('You have not provided any options')
	else:
		samplePath = str(sys.argv[3])
		hecTarget = str(sys.argv[1])
		hecToken = str(sys.argv[2])

        with open(samplePath) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            next(readCSV, None) # skip the header
            lines_read = 0

            for row in readCSV:
                print 'Received line', lines_read
                epoch = row[0]
                index = row[1]
                source = row[2]
                sourcetype = row[3]
                host = row[4]
                raw = row[5]
                print row[0] + row[1] + row[2] + row[3] + row[4] + row[5]

                lines_read = lines_read + 1

                event = raw
                metadata = {"index":"index", "host":host, "source":source, "sourcetype":sourcetype}

                print hec.send(event, metadata)

            # Be a good boy and close the csv file
            csvfile.close()

if __name__ == '__main__':
	main()
